/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2013-2016 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#ifndef IVW_VOLUMESLICESINTENSITY_H
#define IVW_VOLUMESLICESINTENSITY_H

#include <modules/basegl/baseglmoduledefine.h>

#include <inviwo/core/common/inviwo.h>
#include <inviwo/core/datastructures/geometry/geometrytype.h>
#include <inviwo/core/ports/imageport.h>
#include <inviwo/core/ports/volumeport.h>
#include <inviwo/core/processors/processor.h>
#include <inviwo/core/properties/optionproperty.h>
#include <inviwo/core/properties/boolproperty.h>
#include <inviwo/core/properties/compositeproperty.h>
#include <inviwo/core/properties/boolcompositeproperty.h>
#include <inviwo/core/properties/ordinalproperty.h>
#include <inviwo/core/properties/transferfunctionproperty.h>
#include <inviwo/core/properties/eventproperty.h>
#include <modules/opengl/shader/shader.h>
#include <modules/opengl/inviwoopengl.h>

namespace inviwo {
    
    class Mesh;
    
    /** \docpage{org.inviwo.VolumeSlicesIntensities, Volume Slices Intensities}
     * ![](org.inviwo.VolumeSlicesIntensities.png?classIdentifier=org.inviwo.VolumeSlicesIntensities)
     * This processor extracts an arbitrary 2D slice from an input volume.
     *
     * ### Inports
     *   * __volume__ The input volume
     *
     * ### Outports
     *   * __outport__ The extracted volume slice
     *
     * ### Properties
     *   * __Plane Normal__ Defines the normal of the slice plane
     *   * __Plane Position__ Defines the origin of the slice plane
     *   * __World Position__ Outputs the world position of the slice plane (read-only)
     */
    
    /**
     * \class VolumeSlicesIntensities
     * \brief extracts an arbitrary 2D slice from an input volume
     */
    class IVW_MODULE_BASEGL_API VolumeSlicesIntensity : public Processor {
    public:
        VolumeSlicesIntensity();
        virtual ~VolumeSlicesIntensity();
        
        virtual const ProcessorInfo getProcessorInfo() const override;
        static const ProcessorInfo processorInfo_;
        
        virtual void initializeResources() override;
        
        // Overridden to be able to turn off interaction events.
        virtual void invokeEvent(Event*) override;
        
        bool positionModeEnabled() const { return posPicking_.get(); }
        
        // override to do member renaming.
        virtual void deserialize(Deserializer& d) override;
        
    protected:
        virtual void process() override;
        
        void shiftSlice(int);
        
        void modeChange();
        void planeSettingsChanged();
        void updateMaxSliceNumber();
        
        void renderPositionIndicator();
        void updateIndicatorMesh();
        
        // updates the selected position, pos is given in normalized viewport coordinates, i.e. [0,1]
        void setVolPosFromScreenPos(vec2 pos);
        vec2 getScreenPosFromVolPos();
        
        vec3 convertScreenPosToVolume(const vec2 &screenPos, bool clamp = true) const;
        
        void invalidateMesh();
        
        void sliceChange();
        void positionChange();
        void rotationModeChange();
        
        
    private:
        void eventShiftSlice(Event*);
        void eventSetMarker(Event*);
        void eventStepSliceUp(Event*);
        void eventStepSliceDown(Event*);
        void eventGestureShiftSlice(Event*);
        void eventUpdateMousePos(Event*);
        
        VolumeInport inport_;
        ImageOutport outport_;
        Shader shader_;
        Shader indicatorShader_;
        
        CompositeProperty trafoGroup_;
        CompositeProperty pickGroup_;
        CompositeProperty tfGroup_;
        
        OptionPropertyInt sliceAlongAxis_;  // Axis enum
        IntProperty sliceX_;
        IntProperty sliceY_;
        IntProperty sliceZ_;
        
        FloatVec3Property worldPosition_;
        
        FloatVec3Property planeNormal_;
        FloatVec3Property planePosition_;
        FloatProperty imageScale_;
        OptionPropertyInt rotationAroundAxis_;  // Clockwise rotation around slice axis
        FloatProperty imageRotation_;
        BoolProperty flipHorizontal_;
        BoolProperty flipVertical_;
        OptionPropertyInt volumeWrapping_;
        FloatVec4Property fillColor_;
        
        BoolProperty posPicking_;
        BoolProperty showIndicator_;
        FloatVec4Property indicatorColor_;
        
        BoolProperty tfMappingEnabled_;
        TransferFunctionProperty transferFunction_;
        FloatProperty tfAlphaOffset_;
        
        BoolCompositeProperty sampleQuery_;
        FloatVec4Property normalizedSample_;
        FloatVec4Property volumeSample_;
        
        BoolProperty handleInteractionEvents_;
        
        EventProperty mouseShiftSlice_;
        EventProperty mouseSetMarker_;
        EventProperty mousePositionTracker_;
        
        EventProperty stepSliceUp_;
        EventProperty stepSliceDown_;
        
        EventProperty gestureShiftSlice_;
        
        std::unique_ptr<Mesh> meshCrossHair_;
        
        bool meshDirty_;
        bool updating_;
        
        mat4 sliceRotation_;
        mat4 inverseSliceRotation_; // Used to calculate the slice "z position" from the plain point. 
        size3_t volumeDimensions_;
        mat4 texToWorld_;

    };
}

#endif  // IVW_VOLUMESLICESINTENSITY_H
