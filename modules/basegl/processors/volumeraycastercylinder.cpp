/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2012-2016 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#include "volumeraycastercylinder.h"
#include <inviwo/core/ports/imageport.h>
#include <inviwo/core/io/serialization/serialization.h>
#include <inviwo/core/io/serialization/versionconverter.h>
#include <inviwo/core/interaction/events/keyboardevent.h>
#include <modules/opengl/volume/volumegl.h>
#include <modules/opengl/shader/shader.h>
#include <modules/opengl/texture/textureunit.h>
#include <modules/opengl/texture/textureutils.h>
#include <modules/opengl/shader/shaderutils.h>
#include <modules/opengl/volume/volumeutils.h>
#include <inviwo/core/common/inviwoapplication.h>
#include <inviwo/core/util/rendercontext.h>
#include <modules/base/algorithm/image/layermaxpos.h>
#include "inviwo/core/datastructures/image/layerram.h"
#include "volumeslicegl.h"

namespace inviwo {
    
    const ProcessorInfo VolumeRaycasterCylinder::processorInfo_{
        "org.inviwo.VolumeRaycasterCylinder",  // Class identifier
        "Volume Raycaster Cylinder",            // Display name
        "Volume Rendering",            // Category
        CodeState::Experimental,             // Code state
        Tags::GL                       // Tags
    };
    
    VolumeRaycasterCylinder::VolumeRaycasterCylinder()
    : Processor()
    , shader_("raycastingcylinder.frag", false)
    , volumePort_("volume")
    , entryPort_("entry")
    , exitPort_("exit")
    , houghImage_("hough")
    , outport_("outport")
    , transferFunction_("transferFunction", "Transfer function", TransferFunction(), &volumePort_)
    , channel_("channel", "Render Channel")
    , raycasting_("raycaster", "Raycasting")
    , camera_("camera", "Camera")
    , lighting_("lighting", "Lighting", &camera_)
    , toggleShading_("toggleShading", "Toggle Shading", [this](Event* e) { toggleShading(e); },
                     IvwKey::L)
    , debugColor_("debugColor", "Color", vec4(1.0f, 0.0f, 0.0f, 1.0f), vec4(0.0f), vec4(1.0f))
    , planeNormal_("planeNormal", "Plane Normal", vec3(1.f, 0.f, 0.f), vec3(-1.f, -1.f, -1.f),
                   vec3(1.f, 1.f, 1.f), vec3(0.01f, 0.01f, 0.01f))
    , planePosition_("planePosition", "Plane Position", vec3(0.5f), vec3(0.0f), vec3(1.0f))
    , radius_("radius", "Radius", 20, 5, 4096)
    , drawNormal_("drawNormal", "Draw Normal", false)
    , drawCenter_("drawCenter", "Draw Circle Center", false)
    , drawPlane_("drawPlane", "Draw Plane", false)
    , drawCylinder_("drawCylinder", "Draw Cylinder", false)
    , length_("implantLength", "Implant Length", 0.0, 0.0, 1.0)
    , posNormal_("posNormal", "Position on Normal", 0.0, 0.0, 1.0)
    , basis_("Basis", "Basis and offset")
    , sliceRotationProperty_("sliceRotation", "Slice Rotation", mat4()) {
        
        shader_.onReload([this]() { invalidate(InvalidationLevel::InvalidResources); });
        
        addPort(volumePort_, "VolumePortGroup");
        addPort(entryPort_, "ImagePortGroup1");
        addPort(exitPort_, "ImagePortGroup1");
        addPort(houghImage_, "ImagePortGroup1");
        addPort(outport_, "ImagePortGroup1");
        
        channel_.addOption("Channel 1", "Channel 1", 0);
        channel_.setSerializationMode(PropertySerializationMode::All);
        channel_.setCurrentStateAsDefault();
        
        volumePort_.onChange(this, &VolumeRaycasterCylinder::onVolumeChange);
        
        // change the currently selected channel when a pre-computed gradient is selected
        raycasting_.gradientComputationMode_.onChange([this]() {
            if (channel_.size() == 4) {
                if (raycasting_.gradientComputationMode_.isSelectedIdentifier("precomputedXYZ")) {
                    channel_.set(3);
                } else if (raycasting_.gradientComputationMode_.isSelectedIdentifier("precomputedYZW")) {
                    channel_.set(0);
                }
            }
        });
        
        addProperty(drawCenter_);
        addProperty(drawNormal_);
        addProperty(drawPlane_);
        addProperty(drawCylinder_);
        debugColor_.setSemantics(PropertySemantics::Color);
        addProperty(debugColor_);
        addProperty(planeNormal_);
        addProperty(planePosition_);
        addProperty(radius_);
        addProperty(length_);
        addProperty(posNormal_);
        addProperty(channel_);
        addProperty(transferFunction_);
        addProperty(raycasting_);
        addProperty(camera_);
        addProperty(lighting_);
        addProperty(toggleShading_);
        addProperty(basis_);
        
        sliceRotationProperty_.setVisible(false);
        addProperty(sliceRotationProperty_);
    }
    
    const ProcessorInfo VolumeRaycasterCylinder::getProcessorInfo() const {
        return processorInfo_;
    }
    
    void VolumeRaycasterCylinder::initializeResources() {
        utilgl::addShaderDefines(shader_, raycasting_);
        utilgl::addShaderDefines(shader_, camera_);
        utilgl::addShaderDefines(shader_, lighting_);
        shader_.build();
    }
    
    void VolumeRaycasterCylinder::onVolumeChange() {
        if (volumePort_.hasData()) {
            size_t channels = volumePort_.getData()->getDataFormat()->getComponents();
            
            if (channels == channel_.size()) return;
            
            std::vector<OptionPropertyIntOption> channelOptions;
            for (size_t i = 0; i < channels; i++) {
                channelOptions.emplace_back("Channel " + toString(i+1), "Channel " + toString(i+1),
                                            static_cast<int>(i));
            }
            channel_.replaceOptions(channelOptions);
            channel_.setCurrentStateAsDefault();
        }
    }
    
    void VolumeRaycasterCylinder::process() {
        if (volumePort_.isChanged()) {
            auto newVolume = volumePort_.getData();
            
            if (newVolume->hasRepresentation<VolumeGL>()) {
                loadedVolume_ = newVolume;
            } else {
                dispatchPool([this, newVolume]() {
                    RenderContext::getPtr()->activateLocalRenderContext();
                    newVolume->getRep<kind::GL>();
                    glFinish();
                    dispatchFront([this, newVolume]() {
                        loadedVolume_ = newVolume;
                        invalidate(InvalidationLevel::InvalidOutput);
                    });
                });
            }
        }
        
        if (!loadedVolume_) return;
        if (!loadedVolume_->hasRepresentation<VolumeGL>()) {
            LogWarn("No GL rep !!!");
            return;
        }
        
        // Matrix that is used for the Volume Transform
        mat3 basisMat = mat3(basis_.a_.get(), basis_.b_.get(), basis_.c_.get());
        
        // Calculate the length difference of the normal after basisMat transformation
        vec3 norm = planeNormal_.get();
        norm = norm*basisMat;
        float lDiff = glm::length(norm) / glm::length(planeNormal_.get());
        // Apply the length difference to the Implant Length for removing scaling issues
        float implantLength = lDiff * length_.get();
        float posNormal = (1/lDiff) * posNormal_.get();
        
        if (houghImage_.isChanged()) {
            // Get the Layer of the Hough Space Image
            auto img = houghImage_.getData()->getColorLayer()->getRepresentation<LayerRAM>();
            
            // Get the Circle Center on the Hough Image
            pos_ = util::layerMaxPosition(img);
            // Get the Slice number of the Current Slice
            slice = (glm::inverse(sliceRotationProperty_.get()) * vec4(planePosition_.get(), 1.0f)).z;
            // Calculate the 3D Position of the Circle Center
            posCircleCenter3D_ = (sliceRotationProperty_.get() * vec4(pos_.x,pos_.y,slice,1)).xyz();
        }
        
        utilgl::activateAndClearTarget(outport_);
        shader_.activate();
        
        auto dim = houghImage_.getData()->getColorLayer()->getRepresentation<LayerRAM>()->getDimensions();
        
        TextureUnitContainer units;
        utilgl::bindAndSetUniforms(shader_, units, *loadedVolume_, "volume");
        utilgl::bindAndSetUniforms(shader_, units, transferFunction_);
        utilgl::bindAndSetUniforms(shader_, units, entryPort_, ImageType::ColorDepthPicking);
        utilgl::bindAndSetUniforms(shader_, units, exitPort_, ImageType::ColorDepth);
        utilgl::setUniforms(shader_, outport_, camera_, lighting_, raycasting_, channel_, debugColor_, planeNormal_, planePosition_, drawNormal_, drawPlane_, drawCylinder_, drawCenter_);
        shader_.setUniform("radius", (((float)radius_.get())/glm::min(dim.x, dim.y)));
        shader_.setUniform("centerPos", posCircleCenter3D_);
        shader_.setUniform("slice", slice);
        shader_.setUniform("basisMat", basisMat);
        shader_.setUniform("implantLength", implantLength);
        shader_.setUniform("posNormal", posNormal);
        
        utilgl::singleDrawImagePlaneRect();
        
        shader_.deactivate();
        utilgl::deactivateCurrentTarget();
    }
    
    void VolumeRaycasterCylinder::toggleShading(Event*) {
        if (lighting_.shadingMode_.get() == ShadingMode::None) {
            lighting_.shadingMode_.set(ShadingMode::Phong);
        } else {
            lighting_.shadingMode_.set(ShadingMode::None);
        }
    }
    
    // override to do member renaming.
    void VolumeRaycasterCylinder::deserialize(Deserializer& d) {
        util::renamePort(d, {{&entryPort_, "entry-points"}, {&exitPort_, "exit-points"}});
        Processor::deserialize(d);
    }
}  // namespace
