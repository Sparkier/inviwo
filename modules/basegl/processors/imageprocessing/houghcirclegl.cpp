/*********************************************************************************
*
* Inviwo - Interactive Visualization Workshop
*
* Copyright (c) 2013-2016 Inviwo Foundation
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*********************************************************************************/

#include "houghcirclegl.h"
#include <modules/opengl/texture/textureutils.h>
#include "inviwo/core/datastructures/image/layerram.h"

namespace inviwo {

	const ProcessorInfo HoughCircleGL::processorInfo_{
		"org.inviwo.HoughCircleGL",   // Class identifier
		"Hough Circles",       // Display name
		"Image Operation",        // Category
		CodeState::Experimental,        // Code state
		Tags::GL,                 // Tags
	};

	const ProcessorInfo HoughCircleGL::getProcessorInfo() const {
		return processorInfo_;
	}

	HoughCircleGL::HoughCircleGL()
		: ImageGLProcessor("img_houghcircles.frag")
		, radius_("radius", "Radius", 20, 5, 4096)
		, threshold_("edgethreshold", "Edge Threshold", 1.0, 0.0, 1.0) {
		addProperty(radius_);
		addProperty(threshold_);
	}

	HoughCircleGL::~HoughCircleGL() = default;

	void HoughCircleGL::preProcess(TextureUnitContainer &cont) {
		auto dim = inport_.getData()->getColorLayer()->getRepresentation<LayerRAM>()->getDimensions();
		radius_.setMaxValue(glm::min(dim.x, dim.y));
		shader_.setUniform("radius_", (float)radius_.get());
		shader_.setUniform("threshold_", threshold_.get());
		shader_.setUniform("dims_", vec2(dim.x, dim.y));
	}
}  // namespace