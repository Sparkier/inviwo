/*********************************************************************************
*
* Inviwo - Interactive Visualization Workshop
*
* Copyright (c) 2013-2016 Inviwo Foundation
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
* list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
* this list of conditions and the following disclaimer in the documentation
* and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*********************************************************************************/

#include "drawhoughcircle.h"
#include <modules/opengl/texture/textureutils.h>
#include <modules/base/algorithm/image/layermaxpos.h>
#include "inviwo/core/datastructures/image/layerram.h"

namespace inviwo {

	const ProcessorInfo DrawHoughCircle::processorInfo_{
		"org.inviwo.DrawHoughCircle",   // Class identifier
		"Draw Hough Circles",			// Display name
		"Image Operation",				// Category
		CodeState::Experimental,        // Code state
		Tags::GL,						// Tags
	};

	const ProcessorInfo DrawHoughCircle::getProcessorInfo() const {
		return processorInfo_;
	}

	DrawHoughCircle::DrawHoughCircle()
		: ImageGLProcessor("img_drawhoughcircle.frag")
		, radius_("radius", "Radius", 20, 5, 4096)
		, circleColor_("circleColor", "Color", vec4(1.0f, 0.0f, 0.0f, 1.0f), vec4(0.0f), vec4(1.0f))
		, imageHough_("imageHough") {
		addProperty(radius_);
		circleColor_.setSemantics(PropertySemantics::Color);
		addProperty(circleColor_);
		addPort(imageHough_);
	}

	DrawHoughCircle::~DrawHoughCircle() = default;

	void DrawHoughCircle::preProcess(TextureUnitContainer &cont) {
		// Get the Layer of the Hough Space Image
		auto img = imageHough_.getData()->getColorLayer()->getRepresentation<LayerRAM>();
		// Get the Position of the Circles center
		auto pos = util::layerMaxPosition(img);
        // Get the Dimensions of the input Image
        auto dim = inport_.getData()->getColorLayer()->getRepresentation<LayerRAM>()->getDimensions();

		shader_.setUniform("radius_", (float)radius_.get());
		shader_.setUniform("maxPos_", pos);
		shader_.setUniform("dims_", vec2(dim.x, dim.y));
		shader_.setUniform("circleColor_", circleColor_.get());
	}
}  // namespace
