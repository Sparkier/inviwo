/*********************************************************************************
 *
 * Inviwo - Interactive Visualization Workshop
 *
 * Copyright (c) 2013-2016 Inviwo Foundation
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************************/

#include "implantlengthoverlay.h"
#include <modules/opengl/texture/textureutils.h>
#include <modules/base/algorithm/image/layermaxpos.h>
#include "inviwo/core/datastructures/image/layerram.h"

namespace inviwo {
    
    const ProcessorInfo ImplantLengthOverlay::processorInfo_{
        "org.inviwo.ImplantLengthOverlay",   // Class identifier
        "Implant Length Overlay",			// Display name
        "Image Operation",				// Category
        CodeState::Experimental,        // Code state
        Tags::GL,						// Tags
    };
    
    const ProcessorInfo ImplantLengthOverlay::getProcessorInfo() const {
        return processorInfo_;
    }
    
    ImplantLengthOverlay::ImplantLengthOverlay()
    : ImageGLProcessor("img_implantlengthoverlay.frag")
    , length_("length", "Length", 0.0, 0.0, 1.0)
    , posNormal_("posNormal", "Position on Normal", 0.0, 0.0, 1.0)
    , overlayColor_("overlayColor", "Color", vec4(1.0f, 0.0f, 0.0f,0.5f), vec4(0.0f), vec4(1.0f)) {
        addProperty(length_);
        addProperty(posNormal_);
        overlayColor_.setSemantics(PropertySemantics::Color);
        addProperty(overlayColor_);
    }
    
    ImplantLengthOverlay::~ImplantLengthOverlay() = default;
    
    void ImplantLengthOverlay::preProcess(TextureUnitContainer &cont) {
        // Get the Layer of the Hough Space Image
        auto img = inport_.getData()->getColorLayer()->getRepresentation<LayerRAM>();
        // Get the domensions
        auto dim = img->getDimensions();
        
        shader_.setUniform("length_", length_.get());
        shader_.setUniform("posNormal_", posNormal_.get());
        shader_.setUniform("overlayColor_", overlayColor_.get());
        shader_.setUniform("dims_", vec2(dim.x, dim.y));
    }
}  // namespace
